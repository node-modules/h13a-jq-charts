const packageJson = require("./package.json");
const packageJsonMain = require("../../package.json");

const widgetNamespace = packageJsonMain.name.toLowerCase().replace(/-/g,'');
const widgetName = packageJson.name.toLowerCase().replace(/-/g,'');
const widgetFullName = `${widgetNamespace}.${widgetName}`;

require("h13a-strategy3"); // Simple 1+2 Strategy, initialize and then display ... display ... display
const widgetIntegrationStrategy = $[widgetNamespace].h13astrategy3;

const Promise = require("bluebird");

const Chart = require("chart.js");
Chart.defaults.global.responsive = true;

const defaults = require("./defaults.es6");




  // radio --channel chart1update --action merge --path data.datasets.0.points.0 --value 23
  // radio --channel chart1update --action dot --path data.datasets.0.data.0 --value 23

const functions = {

  _updateSimple: function(){
    // copy properties, from options
    this.option('data').forEach((segmentObject, segmentIndex)=>{
      try {
        for (var propertyName in segmentObject) {

            let propertyValue = segmentObject[propertyName];
            // strings only
            if(typeof propertyValue === 'string'){
              this.chart.segments[segmentIndex][propertyName] =  this.option('data')[segmentIndex][propertyName];
            }else{
              //console.log(`Skipped ${propertyName} becasue not a string (${typeof propertyValue})`)
            }

        } // for every segment
      } catch(e){
        console.log(e);
      }
    });

    // update segments
    this.option('data').forEach((segmentObject, segmentIndex)=>{
        try {
          this.chart.segments[segmentIndex].value = this.option("data")[segmentIndex].value;
        } catch(e){
          console.log(e);
        }
    });
  },

  _updateComplex: function(){

    // copy properties, from options
    // iterate over existing chart information.
    this.chart.datasets.forEach((datasetObject, datasetIndex)=>{
      try {

        for (var propertyName in datasetObject) {

            let propertyValue = datasetObject[propertyName];
            // strings only
            if(typeof propertyValue === 'string'){


              let newValue = this.option('data').datasets[datasetIndex][propertyName];

              if(newValue){

                if(newValue != propertyValue){

                  console.log(`CHANGING DATATASET ${datasetIndex} PROPERTY ${propertyName} from ${this.chart.datasets[datasetIndex][propertyName]} to ${propertyValue} `)
                  this.chart.datasets[datasetIndex][propertyName] = newValue;

                }
              }

           }else{
             //console.log(`Skipped ${propertyName} becasue not a string (${typeof propertyValue})`)
           }

        }

      } catch(e){
        console.log(e);
      }
    });

    // copy values, from options
    this.chart.datasets.forEach((datasetObject, datasetIndex)=>{
      if(datasetObject.points) datasetObject.points.forEach((pointObject, pointIndex)=>{
        try {
          this.chart.datasets[datasetIndex].points[pointIndex].value = this.option("data").datasets[datasetIndex].data[pointIndex];
        } catch(e){
          console.log(e);
        }
      });
    });

  },


  _barType: function({ctx, options, data}){
    this.chart = new Chart(ctx).Bar(data, options);
    this.chartDataUpdater = ()=>{
      this._updateComplex();
      this.chart.update();
    }
  },

  _radarType: function({ctx, options, data}){
    this.chart = new Chart(ctx).Radar(data, options);
    this.chartDataUpdater = ()=>{
      this._updateComplex();
      this.chart.update();
    }
  },

  _polarType: function({ctx, options, data}){
    this.chart = new Chart(ctx).PolarArea(data, options);
    this.chartDataUpdater = ()=>{
      this._updateSimple();
      this.chart.update();
    }
  },

  _lineType: function({ctx, options, data}){
    this.chart = new Chart(ctx).Line(data, options);
    this.chartDataUpdater = ()=>{
      this._updateComplex();
      this.chart.update();
    }
  },

  _pieType: function({ctx, options, data}){
    this.chart = new Chart(ctx).Pie(data,options);
    this.chartDataUpdater = ()=>{
      this._updateSimple();
      this.chart.update();
    }

  },

  _doughnutType: function({ctx, options, data}){
    this.chart = new Chart(ctx).Doughnut(data,options);
    this.chartDataUpdater = ()=>{
      this._updateSimple();
      this.chart.update();
    }

  },

    // <<YOUR FUNCTIONS HERE>>

}

const integration = $.extend({
  options: defaults,

  _destroy: function(){
    // The public destroy() method cleans up all common data, events, etc. and then delegates out to _destroy() for custom, widget-specific, cleanup.
    if(this.chart) this.chart.destroy();
    console.log(`${this.widgetFullName}/${this.uuid}: destructor executed.`)

  },

  // Step1 Notes
  // * Create UI
  // * Called once at start
  // * Call display manually
  _prepare: function () {

    let type = this.option('type');
    let chartCreator = this['_'+type+'Type'].bind(this);

    if(chartCreator) {

      // execute the method
      let $content = $(`<div class="chart-position"></div>`);
      let $canvas = $(`<canvas id="chart-canvas chart-${this.uuid}"></canvas>`);

      $content.append($canvas);
      this.element.append($content);

      // SIZE MONITOR -- older browsers and custom browser may have size issues
      const trimIntervalID = setInterval(()=>{
        var w = $content.innerWidth();
        var W = $canvas.width();
        if(W > w) {
          $canvas.width(w);
          if (this.chart && this.chart.refresh ) this.chart.refresh();
          // console.log("NOT OK", w, W);
        }else{
          //console.log("OK", w, W);
        }
      }, 300);
      this.destructors(()=>clearInterval(trimIntervalID));
      // SIZE MONITOR


      let ctx = $canvas.get(0).getContext("2d");
      this.data = this.option('data');
      this.configuration = this.option('configuration');


      // manual call -- chart is created
      setTimeout(()=>{
        chartCreator({ctx, options:this.configuration, data:this.data});
        this.display();
      },1);
      setTimeout(()=>{
        this.chart.options.animation = true;
      },1000);
      // chart has been displayed on screen

    }

  },

  // Step2 Notes
  // * Display On Screen
  // * This will be called each time options finish changing
  // * Called after _prepare finishes
  // * Called each time options change.
  display: function () {

      try {
        if (this.chartDataUpdater) this.chartDataUpdater();
      } catch(e){
        console.log('ERROR IN CHART!', e.message);
      }
  },

}, functions);

$.widget(widgetFullName, widgetIntegrationStrategy, integration);
